

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class ServerTest {
	@Test
	public void addClientTest() {
		Server.getServer().addClient("Client_Test");
		String[] listeClients = Server.getServer().getlistClients();
		 
		
		int test = -1;
		for(int i = 0; i<listeClients.length; i++){
			if(listeClients[i].equals("Client_Test")){
				test = 1;
			}
		}
		assertEquals(1,test);
		
	}

}
