import java.util.Scanner;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;

public class Client {

	public static void main(String args[]) {
		java.util.Properties props = System.getProperties();

		int status = 0;
		org.omg.CORBA.ORB orb = null;

		try {
			orb = ORB.init(args, props);
			run(orb);
		} catch (Exception ex) {
			ex.printStackTrace();
			status = 1;
		}

		if (orb != null) {
			try {
				orb.destroy();
			} catch (Exception ex) {
				ex.printStackTrace();
				status = 1;
			}
		}

		System.exit(status);
	}

	static void run(ORB orb) {
		org.omg.CORBA.Object obj = null;
		// initialiser l'orb
		try {

			obj = orb.resolve_initial_references("NameService");
		} catch (InvalidName e) {
			e.printStackTrace();
			System.exit(1);
		}
		// cr�er le contexre de naming service
		NamingContext ctx = NamingContextHelper.narrow(obj);

		if (ctx == null) {
			System.out.println("Le composant NameService n'est pas un repertoire");
			System.exit(1);
		}
		// cr�er un tableau de noms
		NameComponent[] name = new NameComponent[1];
		//Initialiser le nom de l'objet distant 
		name[0] = new NameComponent("Connection", "");

		try {
			// R�cup�rer la r�f�rence de l'objet distant � partir du Namiing service
			// resolve on est en train de contacter l'annuaire
			obj = ctx.resolve(name);
		} catch (Exception e) {
			System.out.println("Composant inconnu");
			e.printStackTrace();
			System.exit(1);
		}
		
		// l'interface de notre la on esten train de cr�er le proxy c a d objet le stub
		Connection connection = ConnectionHelper.narrow(obj);
		
		Dialogue dialogueYassine = connection.connect("Yassine");
		Dialogue dialogueAmine = connection.connect("Amine");
		
		
		String[] lesClients = dialogueYassine.getClients();
		
		System.out.println("La liste des clients : ");
		for(int i = 0 ; i<lesClients.length; i++){
			if(!lesClients[i].equals("Yassine"))
				System.out.println(lesClients[i]);
		}
		System.out.println("--------------------");
		dialogueYassine.sendMessage("Yassine", "Amine", "Salut");
		
		System.out.println(dialogueAmine.getMessage());
		
		System.out.println(dialogueYassine.getMessage());
		
		dialogueAmine.sendMessage("Amine", "Yassine", "Salut Yassine");
		System.out.println(dialogueYassine.getMessage());
		
		
	}

}
