
import java.util.ArrayList;
import java.util.Iterator;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;

public class Server {
	ArrayList<String> listeClients = new ArrayList<String>();
	ArrayList<Message> listeMessages = new ArrayList<Message>();
	public static Server serveur;
	
	public static void main(String[] args) {
		java.util.Properties props = System.getProperties();
		int status = 0;
		ORB orb = null;
		try
		{
			orb = ORB.init(args, props);
			run(orb);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			status = 1;
		}
		if(orb != null)
		{
			try
			{
				orb.destroy();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				status = 1;
			}
		}
		System.exit(status);
	}
	static Server getServer(){
		if(serveur==null){
			serveur = new Server();
		}
		return serveur;
	}
	
	public String[] getlistClients(){
		int taille = listeClients.size();
		return (String[])listeClients.toArray(new String[taille]);
	}
	
	public void addClient(String client){
		boolean existeUser = false;
		
		for (int i = 0; i < listeClients.size(); i++) {
			if(listeClients.get(i).equals(client))
				existeUser = true;
		}
		
		if(!existeUser){
			listeClients.add(client);
			System.out.println("l'utilisateur : "+  client+" est connect�.");
		}else{
			System.out.println("ce nom d'utilisateur est d�ja connect� veillez choisir un autre nom d'utilisateur");
		}
	}
	
	public void removeClient(String client){
		listeClients.remove(client);
		System.out.println("d�connexion du client : "+client);
	}
	
	public void sendMessage(String from, String to, String message){
		listeMessages.add(new Message(from,to,message));
	}
	
	
	public String getMessage(String client){
		String msg ="";
		for(int i = 0 ; i<listeMessages.size(); i++){
			if(listeMessages.get(i).getTo().equals(client)){
				msg = listeMessages.get(i).getFrom() + " : " + listeMessages.get(i).getMessage();
			}
		}
		return msg;
	}
	
	
	static int run(ORB orb) throws Exception{
		org.omg.CORBA.Object obj;
		org.omg.PortableServer.POA rootPOA =
		org.omg.PortableServer.POAHelper.narrow(
				orb.resolve_initial_references("RootPOA"));
		
	
		org.omg.PortableServer.POAManager manager = rootPOA.the_POAManager();
	
		Connection_Imp connectionImp = new Connection_Imp();
		obj = rootPOA.servant_to_reference(connectionImp);
		
		Connection connection = ConnectionHelper.narrow(obj);
		
		obj = orb.resolve_initial_references("NameService");
		NamingContext ctx = NamingContextHelper.narrow(obj);
	
		if (ctx==null)
		{
			System.out.println("Le composant NameService n'est pas un repertoire");
		}
		NameComponent[] name = new NameComponent[1];
		name[0] = new NameComponent("Connection","");
		
		ctx.rebind(name, connection);
		
		System.out.println("Objet cree et reference");
		manager.activate();
		orb.run();

		return 0;
	}
	
}
