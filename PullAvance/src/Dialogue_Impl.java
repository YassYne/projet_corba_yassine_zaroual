import java.util.ArrayList;

public class Dialogue_Impl extends DialoguePOA{
	public String nomClient;
	
	public Dialogue_Impl(String nom) {
		nomClient = nom;
	}
	
	@Override
	public String getMessage() {
		ArrayList<Message> listeMsg = new ArrayList<>();
		listeMsg = Server.getServer().listeMessages;
		
		String msg ="";
		for(int i = 0 ; i<listeMsg.size(); i++){
			if(listeMsg.get(i).getTo().equals(this.nomClient)){
				msg = listeMsg.get(i).getFrom() + " : " + listeMsg.get(i).getMessage();
			}else{
				msg = "pas de message pour le moment vous M."+this.nomClient;
			}
		}
		
		return msg;
	}

	

	@Override
	public void sendMessage(String from, String to, String message) {
		ArrayList<Message> listeMsg = new ArrayList<>();
		listeMsg = Server.getServer().listeMessages;
		
		listeMsg.add(new Message(from, to, message));
		
	}

	@Override
	public String[] getClients() {
		ArrayList<String> clients = new ArrayList<>();
		clients = Server.getServer().listeClients;
		
		int size = clients.size();
		return (String[])clients.toArray(new String[size]);
	}
	
	public void afficherUtilisateur(ArrayList<String> list){
		
		System.out.println("--> Liste des utilisateurs : ");
		for (int i = 0; i < list.size(); i++) {
			System.out.println("     "+list.get(i));
		}
	}

}
