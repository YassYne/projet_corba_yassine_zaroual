import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

public class Connection_Imp extends ConnectionPOA{

	@Override
	public  Dialogue connect(String pseudo) {
		
		Server.getServer().addClient(pseudo);
		
		Dialogue_Impl dialogueImpl = new Dialogue_Impl(pseudo);
		Dialogue dialogue = null;
		
		try {
			dialogue = DialogueHelper.narrow(_default_POA().servant_to_reference(dialogueImpl));
		} catch (ServantNotActive | WrongPolicy e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return dialogue;
		
	}

	@Override
	public void disconnect(String pseudo) {
		Server.getServer().removeClient(pseudo);
		
	}

}
