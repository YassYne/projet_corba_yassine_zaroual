import java.util.ArrayList;

public class Emitter_Impl extends EmitterPOA{
	public String nomClient;
	
	public Emitter_Impl(String nom) {
		nomClient = nom;
	}
	
	@Override
	public void sendMessage(String to, String message) {
		ArrayList<Reception> rcvListe = new ArrayList<>();
		rcvListe = Server.getServer().receivers;
		for(int i = 0; i<rcvListe.size();i++){
			if(rcvListe.get(i).getClient().equals(to)){
				String txt = nomClient+" --> "+to;
				rcvListe.get(i).getReceiver().receive(txt, message);
			}
		}
		
	}

}