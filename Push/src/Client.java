
import java.util.Date;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;



import org.omg.CORBA.ORBPackage.InvalidName;

public class Client {
	
	private static ORB orb = null;
	public static Connection connection = null;

	public static void main(String args[]) {
		java.util.Properties props = System.getProperties();

		int status = 0;

		try {
			orb = ORB.init(args, props);
			run(orb);
		} catch (Exception ex) {
			ex.printStackTrace();
			status = 1;
		}

		if (orb != null) {
			try {
				orb.destroy();
			} catch (Exception ex) {
				ex.printStackTrace();
				status = 1;
			}
		}

		System.exit(status);
	}

	static void run(ORB orb) throws Exception {
		org.omg.CORBA.Object obj = null;
		org.omg.PortableServer.POA rootPOA = org.omg.PortableServer.POAHelper
				.narrow(orb.resolve_initial_references("RootPOA"));
		org.omg.PortableServer.POAManager manager = rootPOA.the_POAManager();
		manager.activate();

		try {

			obj = orb.resolve_initial_references("NameService");
		} catch (InvalidName e) {
			e.printStackTrace();
			System.exit(1);
		}

		NamingContext ctx = NamingContextHelper.narrow(obj);

		if (ctx == null) {
			System.out.println("Le composant NameService n'est pas un repertoire");
			System.exit(1);
		}

		NameComponent[] name = new NameComponent[1];

		name[0] = new NameComponent("Connection", "");

		try {
			obj = ctx.resolve(name);
		} catch (Exception e) {
			System.out.println("Composant inconnu");
			e.printStackTrace();
			System.exit(1);
		}
		
		connection = ConnectionHelper.narrow(obj);
		
		Thread t = new ORBRunner(orb);
		t.start();
		
		// Procedure de test push
		
		try {
			
			Receiver receiverYassine = createReceiver();
			Emitter emitterYassine = connection.connect("Yassine", receiverYassine);
			
			Receiver receiverHicham = createReceiver();
			Emitter emitterHicham = connection.connect("Hicham", receiverYassine);
			
			Receiver receiverAmine = createReceiver();
			Emitter emitterAmine = connection.connect("Amine", receiverAmine);
			
			emitterAmine.sendMessage("Yassine", "Salut, �a va ?");
			
			emitterHicham.sendMessage("Amine", "coco");
			
			connection.disconnect("Yassine");
			connection.disconnect("Amine");
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static Receiver createReceiver() throws Exception {

		org.omg.CORBA.Object obj;
		org.omg.PortableServer.POA rootPOA = org.omg.PortableServer.POAHelper
				.narrow(orb.resolve_initial_references("RootPOA"));
		
		Receiver_Imp receiverImpl = new Receiver_Imp();
		obj = rootPOA.servant_to_reference(receiverImpl);
		Receiver receiver = ReceiverHelper.narrow(obj);
		
		return receiver;
	}

}
