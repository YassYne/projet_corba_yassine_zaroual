import java.util.ArrayList;

import ReceiverPackage.clientsHolder;

public class Receiver_Imp extends ReceiverPOA{
	private ArrayList<String> listeClients = new ArrayList<>();
	

	@Override
	public void receive(String from, String message) {
		System.out.println(from + " : "+message);
	}

	@Override
	public void addClient(String client) {
		listeClients.add(client);
		System.out.println("l'utilisateur : "+  client+" est connect�.");
	}

	@Override
	public void remClient(String client) {
		listeClients.remove(client);
		System.out.println("l'utilisateur : "+  client+" est d�connect�.");
	}

	@Override
	public void initClients(String[] clients) {
		for(int i =0 ; i<clients.length; i++){
			listeClients.add(clients[i]);
		}
	}

}
