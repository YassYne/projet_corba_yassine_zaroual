import java.util.ArrayList;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

public class Connection_Imp extends ConnectionPOA{
	

	@Override
	public void disconnect(String pseudo) {

		for(int i = 0; i<Server.getServer().receivers.size();i++){
			if(Server.getServer().receivers.get(i).getClient().equals(pseudo)){
				Server.getServer().receivers.get(i).getReceiver().remClient(pseudo);
			}
		}
		
	}

	@Override
	public Emitter connect(String pseudo, Receiver rcv) {
		Reception reception = new Reception(rcv, pseudo);

		rcv.addClient(pseudo);
		Server.getServer().receivers.add(reception);
		
		Emitter_Impl emitterImpl = new Emitter_Impl(pseudo);
		Emitter emitter = null;
		
		try {
			emitter = EmitterHelper.narrow(_default_POA().servant_to_reference(emitterImpl));
		} catch (ServantNotActive | WrongPolicy e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String[] clients ;
		clients = Server.getServer().getlistClients();
		
		rcv.initClients(clients);
		
		return emitter;
	}

}
