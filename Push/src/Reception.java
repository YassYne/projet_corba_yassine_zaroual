
public class Reception {

	private Receiver receiver;
	private String client;
	
	
	public Reception(Receiver receiver, String client) {
		super();
		this.receiver = receiver;
		this.client = client;
	}
	
	public Receiver getReceiver() {
		return receiver;
	}
	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	
	
	
}
