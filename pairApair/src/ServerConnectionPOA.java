
/**
* ServerConnectionPOA.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Pair_A_Pair.idl
* lundi 25 janvier 2016 09 h 34 CET
*/

public abstract class ServerConnectionPOA extends org.omg.PortableServer.Servant
 implements ServerConnectionOperations, org.omg.CORBA.portable.InvokeHandler
{

  // Constructors

  private static java.util.Hashtable _methods = new java.util.Hashtable ();
  static
  {
    _methods.put ("connect", new java.lang.Integer (0));
    _methods.put ("disconnect", new java.lang.Integer (1));
    _methods.put ("getClient", new java.lang.Integer (2));
  }

  public org.omg.CORBA.portable.OutputStream _invoke (String $method,
                                org.omg.CORBA.portable.InputStream in,
                                org.omg.CORBA.portable.ResponseHandler $rh)
  {
    org.omg.CORBA.portable.OutputStream out = null;
    java.lang.Integer __method = (java.lang.Integer)_methods.get ($method);
    if (__method == null)
      throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);

    switch (__method.intValue ())
    {
       case 0:  // ServerConnection/connect
       {
         String nom = in.read_string ();
         ClientConnection cnx = ClientConnectionHelper.read (in);
         ClientManager mgr = ClientManagerHelper.read (in);
         this.connect (nom, cnx, mgr);
         out = $rh.createReply();
         break;
       }

       case 1:  // ServerConnection/disconnect
       {
         String nom = in.read_string ();
         this.disconnect (nom);
         out = $rh.createReply();
         break;
       }

       case 2:  // ServerConnection/getClient
       {
         String nom = in.read_string ();
         ClientConnection $result = null;
         $result = this.getClient (nom);
         out = $rh.createReply();
         ClientConnectionHelper.write (out, $result);
         break;
       }

       default:
         throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);
    }

    return out;
  } // _invoke

  // Type-specific CORBA::Object operations
  private static String[] __ids = {
    "IDL:ServerConnection:1.0"};

  public String[] _all_interfaces (org.omg.PortableServer.POA poa, byte[] objectId)
  {
    return (String[])__ids.clone ();
  }

  public ServerConnection _this() 
  {
    return ServerConnectionHelper.narrow(
    super._this_object());
  }

  public ServerConnection _this(org.omg.CORBA.ORB orb) 
  {
    return ServerConnectionHelper.narrow(
    super._this_object(orb));
  }


} // class ServerConnectionPOA
