
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;

public class Client {

	public static void main(String args[]) {
		java.util.Properties props = System.getProperties();

		int status = 0;
		org.omg.CORBA.ORB orb = null;

		try {
			orb = ORB.init(args, props);
			run(orb);
		} catch (Exception ex) {
			ex.printStackTrace();
			status = 1;
		}

		if (orb != null) {
			try {
				orb.destroy();
			} catch (Exception ex) {
				ex.printStackTrace();
				status = 1;
			}
		}

		System.exit(status);
	}

	static void run(ORB orb) {
		org.omg.CORBA.Object obj = null;

		try {

			obj = orb.resolve_initial_references("NameService");
		} catch (InvalidName e) {
			e.printStackTrace();
			System.exit(1);
		}

		NamingContext ctx = NamingContextHelper.narrow(obj);

		if (ctx == null) {
			System.out.println("Le composant NameService n'est pas un repertoire");
			System.exit(1);
		}

		NameComponent[] name = new NameComponent[1];

		name[0] = new NameComponent("Dialogue", "");

		try {
			obj = ctx.resolve(name);
		} catch (Exception e) {
			System.out.println("Composant inconnu");
			e.printStackTrace();
			System.exit(1);
		}

		Dialogue dilogue = DialogueHelper.narrow(obj);
		dilogue.connect("Toto");
		dilogue.connect("Titi");
		
		String[] lesClients = dilogue.getClients();
		
		System.out.println("La liste des clients : ");
		for(int i = 0 ; i<lesClients.length; i++){
			if(!lesClients[i].equals("Toto"))
				System.out.println(lesClients[i]);
		}
		
		dilogue.sendMessage("Toto","Titi","Salut");
		
		System.out.println("\n");
		
		if(!dilogue.getMessage("Titi").equals(""))
		System.out.println(dilogue.getMessage("Titi"));

	}

}
