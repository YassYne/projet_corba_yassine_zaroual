import java.util.ArrayList;
import java.util.Iterator;

public class Dialogue_impl extends DialoguePOA{
	
	private ArrayList<String> clients = new ArrayList<>(); 
	private ArrayList<Message> lesMessages = new ArrayList<>(); 
	
	
	@Override
	public void connect(String pseudo) {
		boolean existeUser = false;
		
		for (int i = 0; i < clients.size(); i++) {
			if(clients.get(i).equals(pseudo))
				existeUser = true;
		}
		
		if(!existeUser){
			clients.add(pseudo);
			System.out.println("l'utilisateur : "+  pseudo+" est connect�.");
		}else{
			System.out.println("ce nom d'utilisateur est d�ja connect� veillez choisir un autre nom d'utilisateur");
		}
		
		afficherUtilisateur(clients);
	}

	@Override
	public String getMessage(String pseudo) {
		String msg ="";
		for(int i = 0 ; i<lesMessages.size(); i++){
			if(lesMessages.get(i).getTo().equals(pseudo)){
				msg = lesMessages.get(i).getFrom() + " : " + lesMessages.get(i).getMessage();
			}
		}
		
		return msg;
	}

	@Override
	public void disconnect(String pseudo) {
		System.out.println("l'utilisateur : "+  pseudo+" est d�connect�.");
		
		for (int i = 0; i < clients.size(); i++) {
			if(clients.get(i).equals(pseudo)){
				clients.remove(i);
			}
		}
		
	}
	
	public void afficherUtilisateur(ArrayList<String> list){
		System.out.println("--> Liste des utilisateurs : ");
		for (int i = 0; i < list.size(); i++) {
			System.out.println("     "+list.get(i));
		}
	}

	@Override
	public void sendMessage(String from, String to, String message) {
		
		lesMessages.add(new Message(from, to, message));
		
	}

	@Override
	public String[] getClients() {
		int size = clients.size();
		return (String[])clients.toArray(new String[size]);
	}

}
